// for client side, we use IIFE to execute the application
(function(){
    //everything inside this function is our application

    // create an instance of Angular application/module
    //1st parameter ("firstAPP") is the app's name, 2nd parameter [] is the app's depenencies
    var FirstApp = angular.module("FirstApp", []);
    
    var reverse = function(s){
        return s.split("").reverse().join("");
    }

    //define a function for our controller
    var FirstCtrl = function() {
        //hold a reference of this controlller so that when 'this' changes we are still referncing the controller
        var firstCtrl = this;

        //define first model of this controller/vm
        firstCtrl.myText = "Hello, World";

        //define another model
        firstCtrl.reverseMyText = "";

        //definean event for the click button
        firstCtrl.reverseText = function(){
            firstCtrl.reverseMyText = reverse(firstCtrl.myText);
        }

        //define an event to clear text
        firstCtrl.clearText=function(){
            firstCtrl.reverseMyText="";
        }
    }

    //define a controller call FirstCtrl
    FirstApp.controller("FirstCtrl", [ FirstCtrl ])

})();