// load librarys
var express = require("express");
var path = require("path");

// create instance for the app
var app = express();

// setup the port
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

// define the route
app.use(express.static(path.join(__dirname, "public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components"))); //setting the path for /libs folder

// start the server
app.listen(app.get("port"), function(){
    console.log("Application started at port %d" , app.get("port"))
});